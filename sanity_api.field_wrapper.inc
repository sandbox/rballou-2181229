<?php
/**
 * @file
 * Field wrapper
 */

sanity_api_include('inc', 'sanity_api', 'sanity_api.wrapper');

/**
 * Wrap a field
 */
class SanityAPIFieldWrapper extends SanityAPIWrapper {
  /**
   * Constructor
   */
  public function __construct(&$field) {
    $this->field =& $field;
  }

  /**
   * Check if the field has a value
   *
   * @return bool
   *   TRUE if the field has a value
   */
  public function hasValue() {
    // if the field is an empty array, do nothing
    if (empty($this->field)) {
      return FALSE;
    }

    if (!sanity_api_is_multilingual($this->field)) {
      // if LANGUAGE_NONE is not set or empty, there's no value
      if (!isset($this->field[LANGUAGE_NONE]) || (isset($this->field[LANGUAGE_NONE]) && empty($this->field[LANGUAGE_NONE]))) {
        return FALSE;
      }
    }
    else {
      // check if there is a language specific value
      $has_value = FALSE;
      foreach (field_content_languages() as $language) {
        if (isset($this->field[$language]) && !empty($this->field[$language])) {
          $has_value = TRUE;
          break;
        }
      }

      if (!$has_value) {
        return FALSE;
      }
    }

    return TRUE;
  }
}
