<?php
/**
 * @file
 * stuff
 */

sanity_api_include('inc', 'sanity_api', 'sanity_api.wrapper');

class SanityAPIFormWrapper extends SanityAPIWrapper {

  public $form;

  /**
   * Constructor
   */
  public function __construct(&$form) {
    $this->form =& $form;
  }

  public function entityReferenceValue($value) {
    $name = '';
    $id = '';
    if (isset($value->tid)) {
      $name = $value->name;
      $id = $value->tid;
    }
    elseif (isset($value->nid)) {
      $name = $value->title;
      $id = $value->nid;
    }
    else {
      $this->error = 'Cannot convert object to entity reference value: ' . var_export($value, TRUE);
    }

    return '"' . $name . ' (' . $id . ')"';
  }

  /**
   * Set the form's default value
   */
  public function setDefaultValue($field, $value, $language = LANGUAGE_NONE) {
    $this->error = NULL;

    // coerce the value to an array
    if (!is_array($value)) {
      $value = array($value);
    }

    if (isset($this->form[$field][$language])) {

      $index = 0;
      foreach ($value as $this_value) {
        // if this is an object, assume an entity reference field
        if (is_object($this_value) && isset($this->form[$field][$language][$index]['target_id'])) {
          $this->form[$field][$language][$index]['target_id']['#default_value'] = $this->entityReferenceValue($this_value);
        }
        elseif (isset($this->form[$field][$language]['#default_value'])) {
          $this->form[$field][$language]['#default_value'] = $this_value;
        }
        else {
          $this->form[$field][$language][$index]['#default_value'] = $this_value;
        }
        $index++;
      }

    }
    else {
      $this->error = 'Could not set default value. Field [' . $field . '] or language [' . $language . '] not set';
    }

    return $this;

  }

}
