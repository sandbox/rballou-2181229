<?php

/**
 * Include stuff
 */
function sanity_api_include($type, $module, $name) {
  if (function_exists('module_load_include')) {
    module_load_include($type, $module, $name);
  }
  else {
    require_once $name . '.' . $type;
  }
}

/**
 * Check if the field has a value
 */
function sanity_api_field_has_value($field) {
  sanity_api_include('inc', 'sanity_api', 'sanity_api.field_wrapper');
  return sanity_api_field_wrapper($field)->hasValue();
}

/**
 * Get a form wrapper
 */
function sanity_api_form_wrapper(&$form) {
  sanity_api_include('inc', 'sanity_api', 'sanity_api.form_wrapper');
  return new SanityAPIFormWrapper($form);
}

/**
 * Convert an entity reference field to an array of target ids
 *
 * @param array $entity_field
 *   The entity field to search
 * @param string $language
 *   The language to search, defaults to LANGUAGE_NONE
 */
function sanity_api_get_all_target_ids($entity_field, $language = LANGUAGE_NONE) {
  $targets = array();
  foreach ($entity_field[$language] as $key => $value) {
    array_push($targets, $value['target_id']);
  }
  return $targets;
}

/**
 * Has many languages, will travel.
 */
function sanity_api_is_multilingual($array) {
  $keys = array_keys($array);
  $field_languages = field_content_languages();
  $number_of_languages = 0;
  $languages = array();
  foreach ($keys as $key) {
    // skip "#key" keys
    if (substr($key, 0, 1) == '#') {
      continue;
    }
    if (in_array($key, $field_languages)) {
      $number_of_languages++;
      array_push($languages, $key);
    }
  }

  if ($number_of_languages === 1 && in_array(LANGUAGE_NONE, $languages)) {
    return FALSE;
  }
  elseif ($number_of_languages === 0) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Set a default value
 */
function sanity_api_set_default_value(&$form, $field, $value, $language = LANGUAGE_NONE) {
  $wrapper = sanity_api_form_wrapper($form);
  $wrapper->setDefaultValue($field, $value, $language);
  if ($wrapper->error) {
    throw new Exception($wrapper->error);
  }
  return $wrapper->error === NULL;
}

