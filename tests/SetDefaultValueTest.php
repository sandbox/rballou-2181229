<?php
require_once 'sanity_api.module';

if (!defined('LANGUAGE_NONE')) {
  define('LANGUAGE_NONE', 'und');
}

class SetDefaultValueTest extends PHPUnit_Framework_TestCase {
  /**
   * Set default value in language/default
   */
  public function testSetDefaultValue() {
    $form = array(
      'field_test' => array(
        'und' => array(
          '#default_value' => '',
        ),
      ),
    );
    $answer = 'Test';

    // test it
    sanity_api_set_default_value($form, 'field_test', $answer);
    $this->assertEquals($form['field_test'][LANGUAGE_NONE]['#default_value'], $answer);

  }

  /**
   * Set default value in language/0/default
   */
  public function testSetDefaultValueIn0() {
    $form = array(
      'field_test' => array(
        'und' => array(
          array(
            '#default_value' => '',
          ),
        ),
      ),
    );
    $answer = 'Test';

    // test it
    sanity_api_set_default_value($form, 'field_test', $answer);
    $this->assertEquals($form['field_test'][LANGUAGE_NONE][0]['#default_value'], $answer);
  }

  /**
   * Set default value in language/0/default
   */
  public function testSetMultipleDefaultValues() {
    $form = array(
      'field_test' => array(
        'und' => array(
          array(
            '#default_value' => '',
          ),
          array(
            '#default_value' => '',
          ),
        ),
      ),
    );
    $answer = array(
      'Test',
      'Test1',
    );

    // test it
    sanity_api_set_default_value($form, 'field_test', $answer);
    $this->assertEquals($form['field_test'][LANGUAGE_NONE][0]['#default_value'], $answer[0]);
    $this->assertEquals($form['field_test'][LANGUAGE_NONE][1]['#default_value'], $answer[1]);
  }

  /**
   * Set an entity reference default
   */
  public function testSetEntityReferenceDefaultValueWithNode() {
    $form = array(
      'field_test' => array(
        'und' => array(
          array(
            'target_id' => array(
              '#default_value' => '',
            ),
          ),
        ),
      ),
    );
    $answer = new stdClass();
    $answer->nid = 1;
    $answer->title = 'Test';

    sanity_api_set_default_value($form, 'field_test', $answer);
    $this->assertEquals($form['field_test'][LANGUAGE_NONE][0]['target_id']['#default_value'], '"Test (1)"');
  }

  /**
   * Set an entity reference default
   */
  public function testSetEntityReferenceDefaultValueWithTerm() {
    $form = array(
      'field_test' => array(
        'und' => array(
          array(
            'target_id' => array(
              '#default_value' => '',
            ),
          ),
        ),
      ),
    );
    $answer = new stdClass();
    $answer->tid = 1;
    $answer->name = 'Test';

    sanity_api_set_default_value($form, 'field_test', $answer);
    $this->assertEquals($form['field_test'][LANGUAGE_NONE][0]['target_id']['#default_value'], '"Test (1)"');
  }
}
